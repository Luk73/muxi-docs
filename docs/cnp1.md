---
id: cnp1
title: Primeiros Passos
sidebar_label: Primeiros Passos
---

## Transações CNP

Transações de Cartão Não Presente (CNP) permitem que as empresas digitais vendam de forma conveniente seus produtos ou serviços digitais para clientes em todo o mundo. Nossas APIs oferecem um ambiente seguro para se realizar esse tipo de transação e com isso potencializar seus negócios.

Por meio dos gateways de pagamento, o cliente insere as informações do seu cartão, as quais são enviadas às operadoras de cartão e às instituições financeiras e, a partir daí, o banco confirma se há limite disponível e aprova ou nega a compra em segundos.

### Mas é seguro?

**S-I-M!!** Nós da Muxi sabemos o quanto é ruim sermos enganados. Quando usamos um serviço, não começamos pensando que pode dar errado, que podem nos dar uma volta, e certamente não é isso que oferecemos aos nossos clientes. Temos um compromisso com nossos usuários e parceiros a sempre buscar o mais alto padrão de segurança no que diz respeito a pagamentos. Quer saber como fazemos isso?

* **PCI** -> Seguimos 100% dos padrões definidos pelo PCI. O Payment Card Industry – Data Security Standard (PCI-DSS) é um padrão de segurança, voltado para a indústria de cartões de pagamento, que definem os modelos de segurança através de padrões que devem ser seguidos de forma rigorosa. Essa certificação é necessária para qualquer empresa que armazene, transmita ou processe informações sigilosas de portadores de cartões. Dados como nome, número do cartão e código de segurança devem ser criptografados para a segurança dos portadores de cartões. E como queremos que você se sinta seguro, somos uma empresa totalmente certificada pelo PCI-DSS.

* **Criptografia** -> Todos os dados sensíveis que transitam em nosso gateway são protegidos através dos certificados usados durante a transação. Aqui usamos o HTTPS, que é uma implementação do protocolo HTTP sobre uma camada adicional de segurança que utiliza o protocolo SSL/TLS. Essa camada adicional permite que os dados sejam transmitidos por meio de uma conexão criptografada e que se verifique a autenticidade do servidor e do cliente por meio de certificados digitais.

* **Token** -> Não salvamos os dados do seu cartão, eles são tokenizados. A tokenização permite a utilização dos dados do cartão do portador de forma segura e compatível com as normas do PCI-DSS. O conceito básico consiste em enviar os dados do cartão do cliente diretamente do browser dele para a Muxi e então receber de volta um código (token) que representa o cartão em questão. Leia o subitem Tokenização e esteja por dentro da forma mais segura de efetuar um pagamento hoje em dia.

## Autenticação do Lojista

### Precisamos saber quem é você =)

Para usar nossos serviços, serão necessárias duas coisas importantes:

    * Identificador do lojista
    * Token de Autenticação

### E como eu consigo essas coisas, senhor???

Só ligar para gente! Ao entrar em contato com o time operaconal da Muxi, você gerar para você um número de identificação, que chamamos de Merchant Id, e um token de autenticação, que será usado na inicialização do terminal virtual.

### Fale-me mais…

**Merchant Id** -> é um hash de 34 bits que você guardará com você para sempre! Essa hash vai ser tipo o seu CNPJ, ou seja, um número que me diz que lojista você é. Você **vai usar esse dado no corpo da transação de inicialização**.

**Token de Autenticação** -> Esse dado será usado no campo Authentication do HTTP header da transação de inicialização de acordo com o ambiente que deseja acessar. Após a inicialização, você receberá de resposta o token transacional (que é diferente do token de autenticação!) e passará a usar este token ao invés do token de autenticação nas próximas transações. Para gerar o nosso token de autenticação nos baseamos na especificação **JWT.io**, logo o seu tamanho é variável de acordo com a geração do mesmo.

Veja a seguir um exemplo de comando curl para realizar uma requisição de consulta a ciclos de transação.

```bash
curl --request GET --url https://hmg-transaction-api.muxi.net.br/v1/financial_cycles --header 'Authorization:<token>'
```
## O que é Web Service?

Webservices permitem que duas (ou mais) maquinas se comuniquem via uma rede. Veja um exemplo:



![Exemplo de WebService](/img/webservice.png)

Para uma explicação mais profunda, imagine o seguinte cenário: A sua empresa negocia produtos em dólar, cujo valor flutua diariamente. É extremamente importante que você tenha esses valores atualizados para que você tome decisões que lhe tragam melhores resultados. Você pode contratar um estagiário pra ficar atualizando esses valores manualmente, ou você pode **consumir um serviço** que lhe forneça essa informação automaticamente e com confiabilidade.

Esse serviço é o tal do webservice. Você faz uma pergunta *“Quantos reais valem um dólar hoje?”* e ele lhe responde. Esse processo, a nível de comunicação, dá-se por requisições baseadas no **protocolo HTTP**. Existe uma URL específica, que chamamos de *endpoint*, em um servidor que está sempre esperando pela pergunta e, se ela for feita da maneira correta, ele responde. Chamamos essa URL de endpoint porque ela é sim uma URL, mas ela tem vários “finais” que especificam exatamente onde você quer ir. Imagine como se a URL fosse o endereço de uma rua e o endpoint fosse o número da casa, e dentro de cada casa existe um serviço. Por exemplo, em uma casa você entrega a farinha, ovos, leite e te devolvem um bolo; em outra casa você entrega o tecido, linha, agulha e te devolvem uma roupa. Depois da roupa pronta, você ainda pode voltar na casa para fazer um ajuste, ou adicionar um bordado. Assim como existem casas que podem jogar a roupa fora, se você não vai usar mais. Existem também as casas onde você consulta a quantidades de roupas que já solicitou. Isso tudo será traduzido em verbos. Por exemplo:


* Fazer uma roupa/transação -> POST
* Consultar quantas roupas/transações já foram feitas -> GET
* Alterar/ajustar uma roupa/transação -> PUT
* Excluir/Jogar fora uma roupa/transação -> DELETE

Lembre-se de que a pergunta (ou requisição) deve ser feita corretamente, caso contrário o webservice não vai conseguir entender o pedido. Use esta documentação para conferir se seus dados estão corretos e não há nada faltando. Há também um exemplo de arquivo JSON funcional para download que você pode usar para comparar com o que estiver usando.

## Ferramentas

### Postman

Para agilizar os testes de comunicação com as nossas APIs, pode-se utilizar qualquer cliente HTTP. Aqui na Muxi, nós utilizamos o [Postman](https://www.getpostman.com/); pois além de atender muito bem às nossas necessidades, é mais difundido na comunidade. Esse cliente permite configurar e realizar testes antes mesmo de iniciar o desenvolvimento das integrações como as APIs da Muxi. No caso do Postman, existe uma [documentação](https://documenter.getpostman.com/view/4456678/RWaHyVMX?version=latest) bem desenvolvida explicando como utilizar a ferramenta. Ele também gera para o desenvolvedor exemplos de códigos em algumas linguagens, tais como:

- ASP
- Net
- Java
- PHP
- Ruby
- Python

### JSONLint

Para validar se o objeto JSON gerado é válido de acordo com o padrão, podemos utilizar várias ferramentas. Aqui na Muxi, usamos o [JSONLint](https://jsonlint.com/), por ser web e possuir uma interface simples de ser entendida.

### SDKs

Para quem usa a linguagem Java estamos desenvolvendo SDKs (Kit de desenvolvimento de software) para facilitar a integração com nossas APIs. Aguarde!
