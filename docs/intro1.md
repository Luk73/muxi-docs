---
id: intro1
title: Leia-me
sidebar_label: Leia-me
---

O objetivo desta documentação é orientar o desenvolvedor sobre como integrar com as **APIs da Muxi**, descrevendo as funcionalidades e os métodos a serem utilizados, listando informações a serem enviadas e recebidas através de exemplos e com uma linguagem simples e acessível. Desse modo, será necessário apenas conhecimentos básicos em linguagem de programação para Web, requisições HTTP/HTTPS e manipulação de arquivos JSON para interagir com nossas APIs.

Nesse manual, você também encontrará a referência sobre todas as operações disponíveis na API REST da Muxi. Essas operações devem ser executadas utilizando sua chave específica (token de autorização) nos respectivos endpoints a seguir. Para executar uma operação, combine a URL base do ambiente com o endpoint da operação desejada e envie utilizando o verbo HTTP conforme descrito no item do menu ENVIANDO UMA TRANSAÇÃO. Caso esses termos estejam complexos para você, leia o subitem O QUE É WEB SERVICE?.

Para facilitar a integração, nós disponibilizamos um ambiente de Sandbox onde você pode realizar testes antes de mudar o ambiente para produção. Após o desenvolvimento, o acesso deve ser modificado para o ambiente produtivo.


|                 |                 SANDBOX                 |               PRODUÇÃO            |
| :-------------- | :-------------------------------------- | :-------------------------------- |
| **Requisições** | https://hmg-clearent-gw.muxipay.net.br  |                                   |
| **Consultas**   | https://hmg-transaction-api.muxi.net.br | https://transactions.muxi.com.br/ |
