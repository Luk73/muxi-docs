---
id: cnp2
title: Enviando uma transação
---

## Introdução

### O que é uma Transação? 

Do ponto de vista da aplicação, a transação é uma operação que consiste em uma série de instruções ou procedimentos que devem ser realizados em conjunto. Este tipo de circunstância se deve às instruções que apenas tem sentido quando executadas juntas. Nossa aplicação oferece vários tipo de transações como inicialização, autorização, estorno, tokenicação etc. Toda vez que dados forem enviados para API e algo for respondido, teremos uma transação; porém se valores monetários estiverem envolvidos nessa transação, teremos uma transação financeira.

**Transação financeira** é uma operação comercial que consiste em trocar um bem ou serviço por uma determinada quantidade de dinheiro. O termo também se refere à própria operação técnica do banco de dados que executa uma série de operações. No entanto, o significado mais comum se refere à troca de bens, circunstância própria da atividade econômica. 

**Exemplos de transações financeiras:**

* Autorização / Venda
* Pré-autorização
* Confirmação de Pré-autorização
* Cancelamento de Pré-autorização
* Estorno
* Reembolso
* Recorrência
* Cancelamento de Recorrência
* Transferência
* Cancelamento de Transferência

**Exemplos de transações não financeiras:**

* Inicialização
* Cadastro de Portador
* Tokenização

### O que é necessário para realizar uma transação financeira?
Temos diversos meios de envio de uma transação financeira. Para realizá-la, necessitamos antes gerar um token transacional. Quando receber o token de autenticação, você receberá também uma API KEY. Em posse desses dados, enviará uma mensagem de inicialização, e assim obterá o token transacional que é válido por 1 hora, caso gere outro o anterior é inválidado.

### Calma aí! Me explica melhor o que é o Token Transacional!
Claro! O token transacional é uma chave única que identifica para nossa sistema quem é você! Toda vez que você quiser transacionar, você começa fazendo uma inicialização com o token de autenticação no campo Authentication do HTTP header. A resposta dessa inicialização vai gerar para você o token transacional. Você vai copiar esse token transacional e trocar o token de autenticação que estava no campo Authetication do cabeçalho por esse token novo. Com isso, você vai poder fazer todas as outras transações, veja um exemplo mais a seguir. O token transacional tem validade de 1 hora, sendo assim, ao acabar esse tempo, você precisa inicializar novamente para conseguir um novo token e voltar a transacionar normalmente.

### Header HTTP
Toda requisição precisa de um cabeçalho contendo as informações detalhadas a seguir:

* Content-Type -> Indica o formato do corpo da requisiçao.
* Accept -> Indica o tipo de retorno esperado.
* Authorization -> É o campo preenchido por dois valores possíveis: 
    + Token de Autenticação -> Se for uma inicialização.
    + Token Transacional -> Se for qualquer outra transação, exceto a inicialização.

**Exemplo**

| **Header HTTP** |

| Nome | Valor |
| :----- | :----- | 
| Content-type | application/json |
| Accept | application/json |
| Authorization | *token transacional* ou *token de autenticação*| 


## Inicialização - POST /v1/initialization

### O que é a Inicialização? 

A inicialização é um processo usado para fornecer as configurações necessárias para deixar o terminal virtual operante, e com isso poder realizar as transações desejadas. Não é possível transacionar antes de ocorrer a inicialização, pois ela retorna o token transacional que é um campo necessário para todas as outras transações existirem. 


### Como fazer uma Inicialização? 

Para conseguir inicializar o terminal virtual, você precisa ter o Token de Autenticação e a Api Key. Caso você não tenha ainda essas informações, entre em contato com o time operacional da Muxi que teremos um imenso prazer em ajudar.

Em posse do Token de Autenticação e da Api Key, envie uma requisição HTTP/HTTPS para o endpoint *initialization*

**Requisição**

| **Body** | 

| Nome | Tipo | Obrigatório | Descrição |
| :----- | :----- | :------ | :------ |
| apiKey | String | Sim | Identificador do lojista|  

&nbsp;

**Exemplo**

```json
{
    "initializationRequest": {
       "apiKey": "94a08da1fecbb6e8b46990538c7b50b2a4"
    }
}
```

&nbsp;&nbsp;&nbsp;

**Resposta**

| **Body** | 

| Nome | Tipo | Obrigatório | Descrição |
| :----- | :----- | :------ | :------ |
| code | String | Sim | Identifica se a inicialização ocorreu com sucesso |
| currencyList | List | Sim | Listagem com os tipos de moeda |
| transactionalToken | String | Sim | token transacional válido por 1 hora |

&nbsp;&nbsp;

**Exemplo**

```json
{
    "initializationResponse": {
        "code": "ACCEPTED",
        "currencyList": [ "BRL", "USD" ],
        "transactionalToken": "token transacional"
    }
}
```

## Authorização - POST /v1/authorization

A Autorização é a transação financeira mais tradicional possível. Ela é a nossa famosa venda, ou seja, a troca de um produto ou serviço por um valor monetário. A authorização pode ser estornada ou reembolsada caso haja desistência da venda.

Em posse dos dados necessários, token transacional, devemos realizar uma requisição HTTP/HTTPS para o endpoint _authorization_

**Requisição**

| **Body** |
| Nome | Tipo | Obrigatório | Descrição |
| :----- | :----- | :------ | :------ |
| email | String | Sim | E-mail |
| currency | String | Sim | Código da moeda |
| amount | Number | Sim | Valor |
| tax | Number | Não | Taxa |
| orderId | String | Não | Identificador da compra |
| note | String | Não | Observações sobre a transação |
| cardNumber | String | Sim | Número do cartão |
| cardSecurityCode | String | Sim | Código de segurança do cartão |
| cardExpirationDate | String | Sim | Data de expiração do cartão |
| cardHolderName | String | Sim | Nome do portador do cartão |
| billingAddress.postalCode | String | Não | Cep |
| billingAddress.street | String | Não | Logradouro |
| billingAddress.complement | String | Não | Complemento |
| billingAddress.phone | String | Não | Telefone |

&nbsp;

**Exemplo**

```json
{
  "authorizationRequest": {
    "email": "laura.silva@gmail.com",
    "currency": "BRL",
    "amount": 10.0,
    "tax": 10.0,
    "orderId": "123456",
    "note": "Cobrança por serviço de streaming",
    "cardNumber": "4111111111111111",
    "cardSecurityCode": "099",
    "cardExpirationDate": "122020",
    "cardHolderName": "Laura Silva",
    "billingAddress": {
      "postalCode": "20020100",
      "street": "Rua do Carmo, 43",
      "complement": "Centro",
      "phone": "21222333444"
    }
  }
}
```

&nbsp;&nbsp;&nbsp;

**Resposta**

| **Body** |

| Nome              | Tipo   | Obrigatório | Descrição                                                |
| :---------------- | :----- | :---------- | :------------------------------------------------------- |
| msg               | String | Sim         | Mensagem de resposta                                     |
| code              | String | Sim         | Código de resposta da transação (ex.: ACCEPTED/REJECTED) |
| authorizationCode | String | Sim         | Código de autorização                                    |
| trackingNumber    | String | Sim         | Tracking Number                                          |
| gatewayStan       | String | Sim         | NSU do gateway                                           |
| rrn               | String | Sim         | Número de referência                                     |
| muid              | String | Sim         | Identificador único da Muxi                              |

&nbsp;&nbsp;

**Exemplo**

```json
{
  "authorizationResponse": {
    "msg": "Operation Successful",
    "code": "ACCEPTED",
    "authorizationCode": "099218",
    "trackingNumber": "d4d7cbc9-bf43-481b-b26b-e89aec5ebd5f",
    "gatewayStan": "000008",
    "rrn": "d4d7cbc9-bf43-481b-b26b-e89aec5ebd5f",
    "muid": "7c2cb2e0a9004a8893358b1dd7ae5b1d"
  }
}
```
## Pré-authorização - POST /v1/pre_authorization

A pré-autorização de compra no cartão consiste em uma checagem de limite e/ou saldo do cartão utilizado. Essa transação serve para fazer um bloqueio de limite no valor da pré-autorização e esse valor fica constando como pendende para a administradora. Durante o período em que o valor da pré-autorização não for confirmada, o valor fica indisponível para novas compras.

Em posse dos dados necessários, token transacional, devemos realizar uma requisição HTTP/HTTPS para o endpoint *pre_authorization*

**Requisição**

| **Body** | 

| Nome | Tipo | Obrigatório | Descrição |
| :----- | :----- | :------ | :------ |
| email | String | Sim | Email |
| currency | String | Sim | Moeda |
| amount | Number | Sim | Valor |
| tax | Number | Não | Taxa |
| orderId | String | Não | Identificador da compra |
| note | String | Não | Observação |
| cardNumber | String | Sim | Número do cartão |
| cardSecurityCode | String | Sim | Código de segurança |
| cardExpirationDate | String | Sim | Data de expiração |
| cardHolderName | String | Sim | Nome do portador do cartão |
| billingAddress.postalCode | String  | Não | Cep |
| billingAddress.street | String  | Não | Logradouro |
| billingAddress.complement | String  | Não | Complemento |
| billingAddress.phone | String  | Não | Telefone |

&nbsp;

**Exemplo**

```json
{  
  "preAuthorizationRequest": {
      "email": "laura.silva@gmail.com",
      "currency": "BRL",
      "amount": 10.00,
      "tax": 10.00,
      "orderId": "123456",
      "note": "Cobrança por serviço de streaming",
      "cardHolderName": "Laura Silva",
      "cardNumber": "4111111111111111",
      "cardSecurityCode": "099",
      "cardExpirationDate": "122020",
      "billingAddress": {
          "postalCode": "20020100",
          "street": "Rua do Carmo, 43",
          "complement": "Centro",
          "phone": "21222333444"
      }
  }
}

```

&nbsp;&nbsp;&nbsp;

**Resposta**


| **Body** | 

| Nome | Tipo | Obrigatório | Descrição |
| :----- | :----- | :------ | :------ |
| msg | String | Sim | Mensagem de resposta |
| code | String | Sim | Código de resposta da transação (ex.: ACCEPTED/REJECTED) |
| authorizationCode | String | Sim | Código de autorização |
| trackingNumber | String | Sim | Tracking Number |
| gatewayStan | String | Sim | NSU do gateway |
| rrn | String | Sim | Número de referência |
| muid | String | Sim | Identificador único da Muxi |

&nbsp;&nbsp;

**Exemplo**

```json
{
  "preAuthorizationResponse":  {
      "msg": "Operation Successful",
      "code": "ACCEPTED",
      "authorizationCode": "995346",
      "trackingNumber": "6ee41f5d-62eb-4bb9-9855-2b5dea0223ed",
      "gatewayStan": "000001",
      "rrn": "6ee41f5d-62eb-4bb9-9855-2b5dea0223ed",
      "muid": "7c2cb2e0a9004a8893358b1dd7ae5b1d"
  }
}
```
## Cancelamento de Pré-authorização - POST /v1/pre_authorization_void

A pré-autorização de compra no cartão consiste em uma checagem de limite e/ou saldo do cartão utilizado. Essa transação serve para fazer um bloqueio de limite no valor da pré-autorização e esse valor fica constando como pendende para a administradora. Durante o período em que o valor da pré-autorização não for confirmada, o valor fica indisponível para novas compras. 

O Cancelamento de Pré-Autorização representa então a liberação desse limite pré-reservado anteriormente. Caso a pré-autorização seja confirmada, só é possível cancelá-la através do fluxo normal de cancelamento de venda.

Em posse dos dados necessários, token transacional, devemos realizar uma requisição HTTP/HTTPS para o endpoint *pre_authorization_void*

**Requisição**

| **Body** | 

| Nome | Tipo | Obrigatório | Descrição |
| :----- | :----- | :------ | :------ |
| email | String | Sim | E-mail |
| currency | String | Sim | Código da moeda |
| amount | Number | Sim | Valor |
| tax | Number | Não | Taxa |
| originalRrn | String | Sim | Rrn retornado na transação a ser cancelada |
| gatewayStan | String | Sim | Nsu do gateway retornado na transação a ser cancelada |
| muid | String | Sim | Identificador único da Muxi |


&nbsp;

**Exemplo**

```json
{
  "preAuthorizationVoidRequest": {
      "email": "laura.silva@gmail.com",
      "currency": "BRL",
      "amount": 10.00,
      "tax": 10.00,
      "gatewayStan": "000008",
      "originalRrn": "d4d7cbc9-bf43-481b-b26b-e89aec5ebd5f",
      "muid": "bfa6c5285caf4c868941b97b6c15053b"
  }
}
```

&nbsp;&nbsp;&nbsp;

**Resposta**

| **Body** | 

| Nome | Tipo | Obrigatório | Descrição |
| :----- | :----- | :------ | :------ |
| msg | String | Sim | Mensagem de resposta |
| code | String | Sim | Código |
| authorizationCode | String | Sim | Código de autorização |
| gatewayStan | String | Sim | NSU do gateway |

&nbsp;&nbsp;

**Exemplo**

```json
{
  "preAuthorizationVoidResponse": {
    "msg": "Operation Successful",
    "code": "ACCEPTED",
    "authorizationCode": "099218",
    "gatewayStan": "000011" 
  }
}
```
